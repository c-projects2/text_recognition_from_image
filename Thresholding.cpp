/*
Thresholding the image of the text
remember of `pkg-config --libs --cflags opencv4` -llep

g++ Thresholding.cpp `pkg-config --libs --cflags opencv4` -o Threshold -llept -ltesseract
*/

#include <opencv4/opencv2/dnn_superres.hpp>
#include <opencv4/opencv2/opencv.hpp>
#include <vector>
#include <iostream>
#include <type_traits>
#include <cassert>
#include <leptonica/allheaders.h>
#include <tesseract/baseapi.h>
#include <fstream>

#define BINARY_CHANNEL 1
#define BYTES_PER_PIXEL 1


cv::Mat binarize(cv::Mat t_Input){

    //Using OTSU algorith to threshold the input image
    cv::Mat t_BinaryImage;
    cv::cvtColor(t_Input, t_Input,cv::COLOR_BGR2GRAY);
    cv::threshold(t_Input,t_BinaryImage,0,255,cv::THRESH_OTSU);

    //counnting the number of black and white pixels
    unsigned short t_PixelWhite = cv::countNonZero(t_BinaryImage);
    unsigned short t_PixelBlack = t_BinaryImage.size().area() - t_PixelWhite;
    //std::cout << t_BinaryImage.size().area();
    cv::imshow("WIndow",t_BinaryImage);
    return ~t_BinaryImage;
}

std::vector<cv::RotatedRect> findAreas(cv::Mat t_Input){

    //Image must be Binary image
    assert(short(t_Input.channels()) == BINARY_CHANNEL);

    cv::Mat t_Kernel = cv::getStructuringElement(cv::MORPH_CROSS,cv::Size(3,3));
    cv::Mat t_Dilated;

    cv::dilate(t_Input,t_Dilated,t_Kernel,cv::Point(-1,-1),10);

    std::vector<std::vector<cv::Point>> t_Countours;
    cv::findContours(t_Dilated,t_Countours,cv::RETR_EXTERNAL,cv::CHAIN_APPROX_SIMPLE);

    std::vector<cv::RotatedRect> t_Areas;
    for(const auto& c : t_Countours){
        //std::cout << c << std::endl;

        auto box = cv::minAreaRect(c);

        //discards small boxes
        if(box.size.width < 10 || box.size.height < 10){
            continue;
        }

        //proportion
        double proportion = box.angle < -45.0 ?
			box.size.height / box.size.width : 
			box.size.width / box.size.height;
        if(proportion < 2){
            continue;
        }

        t_Areas.push_back(box);

    }
    return t_Areas;
}


cv::Mat DrawingRectangles(cv::Mat t_Image, cv::RotatedRect t_Rect){

    double t_Angle = t_Rect.angle;
    auto t_Size = t_Rect.size;
    cv::imshow("Rectangles",t_Image);

    if(t_Angle < -45.0){
        t_Angle += 90.0;
        std::swap(t_Size.width,t_Size.height);
    }

    //rotate text according to the angle:
    auto t_Transform = cv::getRotationMatrix2D(t_Rect.center,t_Angle,1.0);
    cv::Mat t_Rotated;

    cv::warpAffine(t_Image,t_Rotated, t_Transform,t_Image.size(),cv::INTER_CUBIC);

    //cropping the results:
    cv::Mat t_Cropped;
    cv::getRectSubPix(t_Rotated,t_Size,t_Rect.center,t_Cropped);
    cv::copyMakeBorder(t_Cropped,t_Cropped,10,10,10,10,cv::BORDER_CONSTANT,cv::Scalar(0));
    return t_Cropped;
}

const char* identifyText(cv::Mat t_Input, const char* language = "eng"){
    
    tesseract::TessBaseAPI t_Text;

    //initialization of the tesseract NULL - path to the rootdirectory of tessdata files. Contains language packages
    t_Text.Init("/usr/share/tesseract-ocr/4.00/tessdata", language);
    
    t_Text.SetPageSegMode(tesseract::PSM_SINGLE_BLOCK);
    //One byte per pixel since we  are dealing with the binary images
    t_Text.SetImage(t_Input.data,t_Input.cols,t_Input.rows,BYTES_PER_PIXEL,t_Input.step);

    const char* text_ = t_Text.GetUTF8Text();
    std::cout << "Text: " << text_ << std::endl;
    std::cout << "Confidence: " << t_Text.MeanTextConf() << std::endl;


    return text_;
}


int main(){

   
    //reading image:
    std::cout << "OpenCV version : " << CV_VERSION << std::endl;

    cv::Mat v_TestImage = cv::imread("/home/kacper/MyProjects/TextRecognition/Test.jpg");
    std::ofstream v_File;
    cv::imshow("Cropped text", v_TestImage);
	cv::waitKey(0);	
	cv::destroyWindow("Cropped text");

    v_File.open("Tickets.txt",std::ios::out | std::ios::binary);

    v_TestImage = binarize(v_TestImage);
    std::vector<cv::RotatedRect> v_Boxes = findAreas(v_TestImage);	
    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();

	//For each region
	for (const auto& region : v_Boxes) {
		//Crop 
		auto cropped = DrawingRectangles(v_TestImage, region);
        auto t_Text = identifyText(cropped);

        v_File.write(t_Text,strlen(t_Text));
        v_File << std::endl;
		//Show
		cv::imshow("Cropped text", cropped);
		cv::waitKey(0);	
		cv::destroyWindow("Cropped text");
	}
    v_File.close();
    cv::waitKey(0);
    return 0;
}